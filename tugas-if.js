//IF X = 0 THEN
//        OUTPUT 'GENAP'
//END IF
// -------------------------------------
// let cekangka = prompt('Masukan angka1:');
// 
// if (cekangka%2 === 1) {
//     alert('Angka ganjil')
// } else {
//     alert('Angka genap')
// }
// -----------------------
// let angka = prompt('Masukkan angkanya');

// if (angka === '1') {
//     alert ('Ini angka satu (1)')
// } else if (angka === '2') {
//     alert('Ini angka dua (2)')
// } else if (angka === '3') {
//     alert('Ini angka tiga (3)')
// } else if (angka === '4') {
//     alert('Ini angka empat (4)')
// } else if (angka === '5') {
//     alert('Ini angka lima (5)');
// } else {
//     alert('Ini bukan angka cakupan program');
// }
// ----------------------------
// function seq_checking() {
//     const B = prompt('Please input a number!');
//     const NomorB = parseInt(B);
// 
//     if (NomorB) {
//         alert(`Ini adalah angka ${NomorB}`);
//     } else {
//         alert('Ini bukan angka!');
//     }
// }
// ------------------------------
let angka = Number(prompt('Masukkan angkanya'));
let isAgain;

switch (angka) {
    case 1: 
        alert ('Ini angka satu');
        break;
    case 2: 
        alert ('Ini angka dua');
        break;
    case 3: 
        alert ('Ini angka tiga');
        break;
    case 4: 
        alert ('Ini angka empat');
        break;
    case 5: 
        alert ('Ini angka lima');
        break;
    default: 
        alert ("Ini bukan angka cakupan program");
        isAgain = confirm('Coba lagi?');
        if (isAgain === true) location.reload();
}